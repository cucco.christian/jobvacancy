FROM amd64/ubuntu:xenial

WORKDIR /jobvacancy
COPY . /jobvacancy

RUN apt-get update -y && \
    apt-get install -y curl \
    build-essential git \
    postgresql-9.5 postgresql-contrib postgresql-server-dev-9.5 \
    gnupg2
RUN service postgresql start && \
    su postgres -c "psql --dbname=postgres -f /jobvacancy/create_dev_and_test_dbs.sql"
SHELL ["/bin/bash", "-c"]
RUN gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB && \
    curl -sSL https://get.rvm.io | bash -s stable && \
    source /etc/profile.d/rvm.sh && \
    rvm install 2.5.1 && \
    gem install bundler -v 1.16.1

EXPOSE 3000